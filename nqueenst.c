#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <pthread.h>

typedef struct{
	int x;
	int y;
	int pos;
}queen;

typedef struct{
	queen *queensBoard;
}queue;

unsigned long long result;
int queensNr;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

unsigned long long verifyQueen(queen *queensBoard){
	int x, y;
	unsigned long long sum = 0;
	for(int i = 0; i < queensNr; i++){
		x = queensBoard[i].x;
		y = queensBoard[i].y;
		for(int j = i + 1; j < queensNr; j++){
			if(x == queensBoard[j].x)
				return 0;
			else if(y == queensBoard[j].y)
				return 0;
			else if((x - y) == queensBoard[j].x - queensBoard[j].y)
				return 0;
			else if((x + y) == queensBoard[j].x + queensBoard[j].y)
				return 0;
		}
		sum += (unsigned long long) pow(2, queensBoard[i].pos);
	}

	return sum;

}

int fixQueens(queen *queensBoard, int parametro, int boardSize){
	int solved;
	if(parametro-1 < 0){
		return 1;
	}
	else if(!((queensBoard[parametro-1].pos + 1) == queensBoard[parametro].pos)){
		queensBoard[parametro-1].pos++;
		queensBoard[parametro].pos = queensBoard[parametro-1].pos + 1;
		queensBoard[parametro-1].x = queensBoard[parametro-1].pos / boardSize;
		queensBoard[parametro-1].y = queensBoard[parametro-1].pos % boardSize;
		queensBoard[parametro].x = queensBoard[parametro].pos / boardSize;
		queensBoard[parametro].y = queensBoard[parametro].pos % boardSize;
		return 0;
	}
	else{
		solved = fixQueens(queensBoard, parametro-1, boardSize);
		if(solved == 0){
			queensBoard[parametro].pos = queensBoard[parametro-1].pos + 1;
			queensBoard[parametro].x = queensBoard[parametro].pos / boardSize;
			queensBoard[parametro].y = queensBoard[parametro].pos % boardSize;
			return 0;
		}
	}
	return 1;
}

int incrementBoard(queen *queensBoard, int boardTotal, int queensNumber, int boardSize){
	int condition = 0;
	if(queensBoard[queensNumber-1].pos < (boardTotal - 1)){
		queensBoard[queensNumber-1].pos++;
		queensBoard[queensNumber-1].x = queensBoard[queensNumber-1].pos / boardSize;
		queensBoard[queensNumber-1].y = queensBoard[queensNumber-1].pos % boardSize;
		return 0;
	}
	else{
		condition = fixQueens(queensBoard, queensNumber-1, boardSize);
		return condition;
	}
}

void *process(void *arg){
	queen *board = (queen*) arg;
	unsigned long long test = 0;
	test = verifyQueen(board);
	if(test != 0){
		pthread_mutex_lock(&mutex);
		result += test;
		pthread_mutex_unlock(&mutex);
	}
	pthread_exit(NULL);
}

unsigned long long nqueens(int dim, int queens){

	pthread_t thread[4];
	int boardSize = dim;
	int boardTotal = boardSize * boardSize;
	int queensNumber = queens;
	queensNr = queensNumber;
	queen *queensBoard;
	queue *queue;
	result = 0;
	int stop = 0;

	//tests
	if(queens <= 0 || queens > boardTotal)
		return 0;
	else if(dim <= 0)
		return 0;

	queensBoard = malloc (queensNumber * sizeof(queen));
	queue = malloc (4 * sizeof(queue));
	for(int i = 0; i < 4; i++){
		queue[i].queensBoard = malloc (queensNumber * sizeof(queen));
	}
	
	for(int i = 0; i < queensNumber; i++){
			queensBoard[i].pos = i;
			queensBoard[i].x = queensBoard[i].pos / boardSize;
			queensBoard[i].y = queensBoard[i].pos % boardSize;
	}

	for(int i = 0; i < 4; i++){
			queue[i].queensBoard[0].pos = -1;
			queue[i].queensBoard[0].x = queensBoard[i].pos / boardSize;
			queue[i].queensBoard[0].y = queensBoard[i].pos % boardSize;
	}

	while(stop == 0){
		for(int i = 0; i < 4; i++){
			for(int j = 0; j < queensNumber; j++){
				queue[i].queensBoard[j].pos = queensBoard[j].pos;
				queue[i].queensBoard[j].x = queensBoard[j].x;
				queue[i].queensBoard[j].y = queensBoard[j].y;
			}
			stop = incrementBoard(queensBoard, boardTotal, queensNumber, boardSize);
			if(stop != 0)
				break;
		}
		if(queue[0].queensBoard[0].pos != -1)
			pthread_create(&thread[0], NULL, process, (void*)queue[0].queensBoard);
		if(queue[1].queensBoard[0].pos != -1)
			pthread_create(&thread[1], NULL, process, (void*)queue[1].queensBoard);
		if(queue[2].queensBoard[0].pos != -1)
			pthread_create(&thread[2], NULL, process, (void*)queue[2].queensBoard);
		if(queue[3].queensBoard[0].pos != -1)
			pthread_create(&thread[3], NULL, process, (void*)queue[3].queensBoard);

		if(queue[0].queensBoard[0].pos != -1)
			pthread_join(thread[0], NULL);
		if(queue[1].queensBoard[0].pos != -1)
			pthread_join(thread[1], NULL);
		if(queue[2].queensBoard[0].pos != -1)
			pthread_join(thread[2], NULL);
		if(queue[3].queensBoard[0].pos != -1)
			pthread_join(thread[3], NULL);

		for(int i = 0; i < 4; i++){
			queue[i].queensBoard[0].pos = -1;
			queue[i].queensBoard[0].x = queensBoard[i].pos / boardSize;
			queue[i].queensBoard[0].y = queensBoard[i].pos % boardSize;
		}
	}
	
	return result;

}