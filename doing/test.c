#include <stdio.h>
#include <stdlib.h>

void nqueens(int *board, int *queens, int board_size, int queens_number){

	for(int i = 0; i < queens_number; i++){
		board[queens[i]] = 1;
	}

	for(int i = 0; i < board_size; i++){
		if(i % 2 == 0)
			printf("\n");
		printf("%d ", board[i]);
	}

	printf("\n\n");


}

int fixQueens(int *queens, int parametro){
	if(((parametro - 1) >= 0) && ((queens[parametro - 1] + 1) == queens[parametro])){
		fixQueens(queens, parametro-1);
	}
	else{
		if(parametro-1 < 0){
			return 1;
		}
		else{
			queens[parametro-1]++;
			queens[parametro] = queens[parametro-1] + 1;
			return 0;
		}
	}
}

void main(){

	int board_size = 4;
	int queens_number = 3;
	int *board;
	int *queens;
	int condition = 0;
	
	queens = malloc (queens_number * sizeof(int));
		for(int i = 0; i < queens_number; i++)
				queens[i] = i;

	while(condition == 0){
		board = malloc (board_size * sizeof(int));
		for(int i = 0; i < board_size; i++)
			board[i] = 0;

		nqueens(board, queens, board_size, queens_number);
		free(board);

		if(queens[queens_number-1] < (board_size - 1)){
			queens[queens_number-1]++;
		}
		else{
			condition = fixQueens(queens, queens_number-1);
		}


		//printf("%d %d\n\n", queens[0], queens[1]);
	}
		
		

	/*printf("%d %d\n%d %d", board[0], board[1], board[2], board[3]);
		for(int j = 0; j < board_size; j++)
			board[j] = 0;
		printf("\n\n");*/
}